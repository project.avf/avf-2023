from flask import Flask
import mysql.connector
import os

app = Flask(__name__)

@app.route('/')
def hello_world():
    conn = mysql.connector.connect(
        host="db",
        user="root",
        password="senha",
        database="meubanco"
    )
    cursor = conn.cursor()
    cursor.execute("SELECT mensagem FROM mensagens")

    # Buscar todas as mensagens
    mensagens = cursor.fetchall()
    
    # Construir uma string com todas as mensagens
    todas_mensagens = '<br>'.join([mensagem[0] for mensagem in mensagens])

    conn.close()
    return 'Mensagens do banco de dados: <br>' + todas_mensagens

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=5001)  